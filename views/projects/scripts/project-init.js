var ProjectInit = function(projectId) {
  this.projectId = projectId;
  
  this.COL_TYPE_ORDERS = ["string", "number", "date", "boolean"];
  this.autoDetectedDelimiter = "";
  this.metaFormTemplate;
  this.metadata = [];

  this.fileUploader = new FileUploader();

  this.initEvent();
} 

ProjectInit.prototype.refineParsingResults = function(parsingResults) {
  const self = this;

  var lang = $("#header-lang").val();

  self.autoDetectedDelimiter = parsingResults.meta.delimiter;
  var headers = parsingResults.meta.fields;
  if (!headers) {
    headers = new Array(Object.keys(parsingResults.data[0]).length);
  }

  var cols = [];
  for (var header of headers) {
    var colObj = {
      maxLen: 0,
      name: header,
    };

    if (lang == "ko") {
      colObj.koName = header;
    } else {
      colObj.enName = header;
    }

    cols.push(colObj);
  }

  var isFirst = true;

  for (var rowItem of parsingResults.data) {
    if(!isFirst) {
      for (var col of cols) {
        var value = rowItem[col.name];
        col.type = findType(value);
        if (col.type == "number") {
          var strValue = "" + value;
          col.maxLen = Math.max(10, strValue.length);

          if (strValue.indexOf(".") > -1) {
            col.floatLen = Math.max(5, strValue.split(".")[1].length);
          }
          
        } else if (col.type == "string") {
          col.maxLen = Math.max(50, ("" + value).length + 25);
        } else {
          col.maxLen = Math.max(col.maxLen, ("" + value).length);
        }
      }
    } else {
      isFirst = false;
    }
  }

  return cols;
}

ProjectInit.prototype.renderMetaForm = function(refinedParsingResult) {
  const self = this;

  $.get("/javascripts/templates/data/metaForm.hbs", function(t) {
    var metaFormTemplate = Handlebars.compile(t);
    var metaFormHtml = metaFormTemplate(refinedParsingResult);
  
    $(".csv-cols-wrap").html(metaFormHtml);
    $(".meta-form-select").formSelect({
      classes: "meta-form-select"
    });
    $('.tooltipped').tooltip();
  });
}

ProjectInit.prototype.renderProfilingOption = function() {
  const self = this;
  console.log(self.metadata);
  $.get("/javascripts/templates/data/profilingOptionForm.hbs", function(t) {
    var profilingOptionTemplate = Handlebars.compile(t);
    var profilingOptionHtml = profilingOptionTemplate(self.metadata);
  
    $("#profiling-option-wrap").html(profilingOptionHtml);
    $(".profiling-meta-form-select").formSelect({
      classes: "meta-type-select"
    });
  });
}

ProjectInit.prototype.updateMetaFromInputs = function() {
  const self = this;

  var meta = [];
  $(".meta-form-row").each(function(idx, row) {
    meta.push({
      name: $(row).find(".name").val() || "col" + (idx + 1),
      koName: $(row).find(".ko-name").val(),
      dataType: $(row).find(".data-type").val(),
      maxLength: $(row).find(".max-length").val(),
      floatLength: $(row).find(".float-length").val(),
      dateFormat: $(row).find(".date-format").val(),
      trueValue: $(row).find(".true-value").val(),
      isNotNull: $(row).find(".is-not-null").is(":checked"),
      isUnique: $(row).find(".is-unique").is(":checked"),
      isIndex: $(row).find(".is-index").is(":checked"),
    });
  });

  self.metadata = meta;
}

ProjectInit.prototype.fixColType = function(before, after) {
  const self = this;

  if (!before) {
    return after;
  }
  
  if (!after) {
    return before;
  }

  if (self.COL_TYPE_ORDERS.indexOf(before) < self.COL_TYPE_ORDERS.indexOf(after)) {
    return before;
  }

  return after
}

ProjectInit.prototype.updateProfilingOptionFromInputs = function() {
  const self = this;

  $(".profiling-form-row").each(function(idx, row) {
    self.metadata[idx].isProfiling = $(row).find(".is-profiling").is(":checked");
    self.metadata[idx].dataType = $(row).find(".data-type").val()
  });
}

ProjectInit.prototype.initEvent = function() {
  const self = this;

  $("#extract-btn").click(function(e) {
    var files = self.fileUploader.getFiles();
    if (!files) {
      alert("등록할 파일을 선택해주세요.");
      return;
    }
    var f = files[0];
    var csvConfig = {
      delimiter: $("#delimiter").val(),
      header: $("#has-header").is(":checked"),
      encoding: $("#encoding").val(),
      preview: 10,
      complete: function(results, file) {
        var refinedResult = self.refineParsingResults(results);
        self.renderMetaForm(refinedResult);
      }
    }

    Papa.parse(f, csvConfig);
  });

  $(document).on("click", "#profiling-option-modal-btn", function() {
    self.updateMetaFromInputs();
    self.renderProfilingOption();
    $("#profiling-option-modal").modal('open');
  });

  $(document).on("click", "#save-meta-btn", function() {
    var formData = new FormData();
    formData.append("projectId", self.projectId);
    formData.append('file', self.fileUploader.getFiles()[0]);

    var parseOption = {
      delimiter: $("#delimiter").val() == '' ? self.autoDetectedDelimiter : $("delimiter").val(),
      header: $("#has-header").is(":checked"),
      encoding: $("#encoding").val()
    };
    formData.append('parseOption', JSON.stringify(parseOption));

    self.updateProfilingOptionFromInputs();
    formData.append('meta', JSON.stringify(self.metadata));
    
    $(".loading").addClass("active");

    $.ajax({
      url: '/datasets',
      data: formData,
      type: 'POST',
      contentType: false,
      processData: false,
      success: function(d) {
        $(".loading").removeClass("active");
        window.location.href = "/projects/" + d.projectId;
      }, error: function(err) {
        $(".loading").removeClass("active");
        alert("데이터를 등록 할 수 없습니다.")
        console.error(err);
      }
    })
  });
}

function findType(value) {
  if (!Number.isNaN(value) && !Number.isNaN(Number(value))) {
    return "number";
  } else if (moment(value).isValid()) {
    return "date";
  } else if (value.toUpperCase() === 'TRUE' || value.toUpperCase() === 'FALSE') {
    return "boolean";
  } else {
    return "string";  
  }
}



