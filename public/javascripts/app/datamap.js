d3.selection.prototype.dblTap = function(callback) {
    var last = 0;
    return this.each(function() {
        d3.select(this).on("touchstart", function(e) {
            if ((d3.event.timeStamp - last) < 500) {
                return callback(e);
            }
            last = d3.event.timeStamp;
        });
    });
};

var Datamap = function() {
    this.isLoaded = false;
    this.currentMetaId = 0;
    this.currentMeta = null;
    this.metadata = [];
    this.distanceMatrix = {};
    this.processedData = [];

    this.loadMetaAndDistanceMatrix();

    this.initEvent();
};
Datamap.MAX_SIZE = 30;
Datamap.outerCircleRadiuses = [];
Datamap.searchReltionship = 40;
Datamap.searchDepth = 4;

Datamap.collapseLevel = function(d) {
    var relPoint = d.data.size * 100;

    if (d.children && (d.depth >= Datamap.searchDepth || relPoint <= Datamap.searchReltionship)) {
        d._children = d.children;
        d._children.forEach(Datamap.collapseLevel);
        d.children = null;
    } else if (d.children){
        d.children.forEach(Datamap.collapseLevel);
    }
};

Datamap.directLine = function(d, i) {
    var sourceP = Datamap.radialPoint(d.source.x, d.source.y);
    var targetP = Datamap.radialPoint(d.target.x, d.target.y);

    if (Datamap.outerCircleRadiuses[d.target.depth] == null) {
        Datamap.outerCircleRadiuses[d.target.depth] = Math.sqrt(targetP[0] * targetP[0] + targetP[1] * targetP[1]);
    }

    return "M" + sourceP[0] + "," + sourceP[1]
        +"L" + targetP[0] + "," + targetP[1];
};

Datamap.radialPoint = function(x, y) {
    return [
        (y = +y) * Math.cos(x -= Math.PI / 2), y * Math.sin(x)
    ];
};

Datamap.prototype.loadMetaAndDistanceMatrix = function () {
    var self = this;
    
    $.get("/datamap/distances", function(d) {
        self.metadata = d.projects;
        self.currentMeta = self.metadata[0];
        self.currentMetaId = self.currentMeta.id;

        self.isLoaded = true;
        self.distanceMatrix = d.datamapResult;

        self.makeMapData();
    });
};

Datamap.prototype.makeMapData = function(hasFocus) {
    var self = this;

    if (self.isLoaded) {
        var root = self.currentMeta;

        root.alias = "_" + self.currentMeta.id;

        self.processedData = [];
        self.processedData.push(root.id);

        self.treeData = {
            id: root.id,
            alias: "_" + root.id,
            name: root.title,
            size: Datamap.MAX_SIZE,
            depth: 0,
            storeOrg: "",
            descUrl: "/projects/" + root.id
        };

        //Distacne Matrix에 기재된 alias(txt...) 키 조회
        var aliases = Object.keys(self.distanceMatrix);
        var rootKeyIdx = aliases.indexOf(root.alias);
        aliases.splice(rootKeyIdx, 1);

        var targetData = self.getSortedData(root, 1, 1);
        var firstChildren = targetData.slice(0, 6);
        self.treeData.children = firstChildren;

        _.forEach(firstChildren, function(child) {
            idx = aliases.indexOf("_" + child.id);
            aliases.splice(idx, 1);

            self.processedData.push(child.id);
        });

        _.forEach(self.treeData.children, function(childData) {
            self.makeChildren(aliases, childData, 2);
        });

        _.forEach(self.treeData.children, function(firstChild) {
            _.forEach(firstChild.children, function(secondChild) {
                self.makeChildren(aliases, secondChild, 3);
            });
        });

        // if (!mobilecheck()) {
        //     _.forEach(self.treeData.children, function(firstChild) {
        //         _.forEach(firstChild.children, function(secondChild) {
        //             _.forEach(secondChild.children, function(thirdChild) {
        //                 self.makeChildren(aliases, thirdChild, 4);
        //             });
        //         });
        //     });
        // }

        self.renderDatamap();
        self.explainData(root.id);

        if (hasFocus) {
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#tree-container").offset().top
            }, 500);
        }
    }
};

Datamap.prototype.setMetadatum = function(id) {
    var self = this;

    self.currentMeta = _.find(self.metadata, function (m) {
        return m.id == id;
    });

    self.makeMapData(true);
    self.explainData(id);
};

Datamap.prototype.makeChildren = function(aliases, childData, depth) {
    var self = this;

    childData.children = [];

    var sizeRatio = 1;

    depth = depth || 1;
    if (depth > 2) {
        sizeRatio = 0.5;
    } else if (depth > 3) {
        sizeRatio = 0.3;
    }

    var targetChildren = self.getSortedData(childData, sizeRatio, depth);

    var addedCnt = 0;
    var leafCnt = Math.floor(Math.random() * 3) + 2;

    _.forEach(targetChildren, function(d) {
        var aliasIdx = aliases.indexOf(d.alias);
        if (aliasIdx > -1) {
            d.depth = depth;
            childData.children.push(d);
            addedCnt++;
            aliases.splice(aliasIdx, 1);

            self.processedData.push(d.id);
        }

        if (addedCnt == leafCnt) {
            return false;
        }
    });
};

Datamap.prototype.getSortedData = function(metadatum, sizeRatio, depth) {
    var self = this;

    //alias 기반의 row 조회
    var distanceRow = self.distanceMatrix[metadatum.alias];
    var aliases = Object.keys(distanceRow);
    var data = [];
    sizeRatio = sizeRatio || 1;

    _.forEach(aliases, function(alias) {
        var metaItem = _.find(self.metadata, function(m) {
            return "_" + m.id == alias;
        });

        if (metaItem && self.processedData.indexOf(metaItem.id) == -1) {
            var distance = distanceRow[alias];
            var size = (25 - distance) * (sizeRatio);

            data.push({
                id: metaItem.id,
                alias: "_" + metaItem.id,
                name: metaItem.title,
                distance: distance,
                size: size,
                depth: depth,
                storeOrg: "",
                descUrl: "/projects/" + metaItem.id
            });
        }
    });

    return _.orderBy(data, ['distance'], ['asc']);
};

Datamap.prototype.renderDatamap = function() {
    var self = this;

    var div = d3.select("body").append("div")
        .attr("class", "map-tooltip")
        .style("opacity", 0);

    var svg = d3.select("#datamap");
    var width = parseFloat($("#tree-container").width()) * 0.8, height = width;
    $("#datamap").height(height);

    svg.select("g").remove();
    var g = svg.append("g").attr("transform", "translate(" + (width / 2) + "," + ((height / 2) + 100) + ")");

    var radius = Math.min(width, height) / 2;
    var tree = d3.tree().size([2 * Math.PI, radius])
        .separation(function (a, b) {
            return (a.parent == b.parent ? 1 : 2) / a.depth;
        });

    var nodes = d3.hierarchy(self.treeData, function (d) {
        return d.children;
    });

    var root = tree(nodes);
    root.children.forEach(Datamap.collapseLevel);

    _.forEach(root.links(), function(l) {
        Datamap.directLine(l, 0);
    });

    _.forEach(Datamap.outerCircleRadiuses, function (r) {
        if (r) {
            g.append("circle")
                .attr("r", r)
                .attr("class", "outer-line");
        }
    });

    var link = g.selectAll(".link")
        .data(root.links())
        .enter().append("path")
        .attr("class", function (d) {
            return "link " + d.target.data.relation_type;
        })
        .attr("d", Datamap.directLine);

    var node = g.selectAll(".node")
        .data(root.descendants())
        .enter().append("g")
        .attr("data-id", function (d) {
            return d.data.id;
        })
        .attr("class", function (d) {
            var className = "node";
            if (d.depth && d.depth > 2) {
                className = "node circle-group";
                if (!d.children) {
                    className = className + " leaf";
                }
            }
            return className;
        })
        .attr("transform", function (d) {
            return "translate(" + Datamap.radialPoint(d.x, d.y) + ")";
        });

    node.append("circle")
        .attr("class", function(d) {
            return "node colored";
        })
        .attr("r", function (d) {
            var size = d.data.size;

            // if (mobilecheck()) {
            //     size = size * 0.6;
            // }

            return size;
        })
        .on("mouseover", function(d) {
            var metaItem = _.find(self.metadata, function (m) {
                return m.id == d.data.id;
            });

            div.transition()
                .duration(200)
                .style("opacity", .9);
            var summaryHtml = [];
            summaryHtml.push("Title: <strong>" + metaItem.title + "</strong>");
            if (metaItem.description) {
                summaryHtml.push("<div>Description: " + (metaItem.description || "") + "</div>");
            }
            summaryHtml.push("<div>Keywords: " + (metaItem.keywords || "") + "</div>");

            div	.html(summaryHtml)
                .style("left", (d3.event.pageX) + "px")
                .style("top", (d3.event.pageY - 28) + "px");
        })
        .on("mouseout", function(d) {
            div.transition()
                .duration(500)
                .style("opacity", 0);
        })
    ;

    node.append("text")
        .attr("fill", function (d) {
            var fillStyle = fillStyle = "url(#truncateLegendText0)";
            if (d.depth && d.depth >= 3) {
                fillStyle = "url(#truncateLegendText1)";
            }

            return fillStyle;
        })
        .attr("dy", "0.31em")
        .attr("x", function (d) {
            // return d.x < Math.PI === !d.children ? 6 : 6;
            return d.data.size + 8;
        })
        .attr("text-anchor", function (d) {
            // return d.x < Math.PI === !d.children ? "start" : "start";
            return "start";
        })
        .attr("transform", function (d) {
            var rotate = 320;
            return "rotate(" + rotate + ")";
        })
        .text(function (d) {
            var name = d.data.name;
            if (name.length > 12) {
                name = name.slice(0, 12) + "...";
            }
            return name;
        });

    function clickcancel() {
        // we want to a distinguish single/double click
        // details http://bl.ocks.org/couchand/6394506
        var dispatcher = d3.dispatch('click', 'dblclick');
        function cc(selection) {
            var down, tolerance = 5, last, wait = null, args;
            // euclidean distance
            function dist(a, b) {
                return Math.sqrt(Math.pow(a[0] - b[0], 2), Math.pow(a[1] - b[1], 2));
            }
            selection.on('mousedown', function() {
                down = d3.mouse(document.body);
                last = +new Date();
                args = arguments;
            });
            selection.on('mouseup', function() {
                if (dist(down, d3.mouse(document.body)) > tolerance) {
                    return;
                } else {
                    if (wait) {
                        window.clearTimeout(wait);
                        wait = null;
                        dispatcher.apply("dblclick", this, args);
                    } else {
                        wait = window.setTimeout((function() {
                            return function() {
                                dispatcher.apply("click", this, args);
                                wait = null;
                            };
                        })(), 300);
                    }
                }
            });
        };
        // Copies a variable number of methods from source to target.
        var d3rebind = function(target, source) {
            var i = 1, n = arguments.length, method;
            while (++i < n) target[method = arguments[i]] = d3_rebind(target, source, source[method]);
            return target;
        };

        // Method is assumed to be a standard D3 getter-setter:
        // If passed with no arguments, gets the value.
        // If passed with arguments, sets the value and returns the target.
        function d3_rebind(target, source, method) {
            return function() {
                var value = method.apply(source, arguments);
                return value === source ? target : value;
            };
        }
        return d3rebind(cc, dispatcher, 'on');
    }

    var cc = clickcancel();
    node.call(cc);
    cc.on('click', function(d, index) {
        window.location.assign(d.data.descUrl);
    });
    cc.on('dblclick', function(d, index) {
        self.currentMeta = _.find(self.metadata, function(m) {
            return m.id == d.data.id;
        });
        $("div.map-tooltip").css("opacity", 0);

        self.makeMapData();
    });
};

Datamap.prototype.getTreeObj = function(metaItem) {
    return {
        id: metaItem.id,
        alias: metaItem.alias,
        name: metaItem.title,
        size: Datamap.MAX_SIZE,
        depth: 0,
        storeOrg: metaItem.storeOrg,
        children: []
    }
};

Datamap.prototype.explainData = function(id, hasFocus) {
    var self = this;

    var metaItem = _.find(self.metadata, function(m) {
        return m.id === id;
    });

    metaItem.tagArr = [];
    if (metaItem.tags) {
        metaItem.tagArr = metaItem.tags.split(",");
    }

    metaItem.parent = null;
};

Datamap.prototype.search = function(keyword) {
    var self = this;

    $.ajax({
        type: "GET",
        url: "/api/datamap/search?keyword=" + keyword,
        success: function(resultId) {
            var isContained = _.includes(Object.keys(self.distanceMatrix), "_" + resultId);
            if (!isContained) return;

            self.currentMeta = _.find(self.metadata, function(m) {
                return m.id == resultId;
            });
            $("div.map-tooltip").css("opacity", 0);

            self.makeMapData();
        },
        error: function(e) {
            console.log(e);
        }
    });
}

Datamap.prototype.initEvent = function() {
    var self = this;

    $("#datamap-search-btn").on("click", function(e) {
        e.preventDefault();

        self.search($("#datamap-search-key-input").val());
    });
    $("#datamap-search-key-input").keyup(function(e) {
        if (e.keyCode == 13) {
            self.search($(this).val());
        }
    });
};