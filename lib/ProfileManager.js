const axios = require("axios");
const profileConfig = require("../configs/config").profiler;
const FileManager = require("../lib/FileManager");

const PROFILE_RESULT_S3_KEY = "profile-result.json";

function buildProfileOptions(dataset, option, fileS3DownloadUrl) {
  let params = {
    source: {
      type: "url",
      path: null,
      url: fileS3DownloadUrl 
    },
    profiles: {
      key_analysis: false,
      dependencied_analysis: [],
      fk_analysis: []
    }
  }

  if (option && option.header) {
    params.header = option.header;
  }
  
  if(dataset.metas) {
    let targetColumns = {
      basic: [],
      number: [],
      string: [],
      date: [],
    };

    for (const m of dataset.metas) {
      if (!m.isProfiling) {
        continue;
      }
      targetColumns['basic'].push(m.colSeq+1);
      if (m.colType == 'boolean') {
        targetColumns['string'].push(m.colSeq+1)
      } else {
        targetColumns[m.colType].push(m.colSeq+1)
      }
    }

    params.profiles.column_analysis = targetColumns;
  }

  return params;
}

async function requestProfileV2(dataset, option) {
  const key = dataset.remotePath;
  const fileS3DownloadUrl = await FileManager.findS3ObjectUrl(key);
  const params = buildProfileOptions(dataset, option, fileS3DownloadUrl);

  try {
    console.log(
      `CALL URL: ${profileConfig.baseUrl}/profile/local`
    );
    console.log("=====profile params", params)
    const response = await axios.post(
      `${profileConfig.baseUrl}/profile/local`,
      params
    );

    return response.data;
  } catch {
    console.error("Exception in findProfileResult >>>>>>>>>>>>>");
    console.error(err);
    return null;
  }
  

  // sample response.data
  // const sampleData = {
  //   dataset_name: '1675908266291_250',
  //   dataset_type: 'csv',
  //   dataset_size: 12309,
  //   dataset_column_cnt: 6,
  //   dataset_row_cnt: 250,
  //   single_column_results: [
  //     {
  //       column_id: 1,
  //       column_name: "id",
  //       column_type: "number",
  //       profiles: {
  //         basic_profile: {
  //           row_cnt: 250,
  //           distinct_cnt: 250,
  //           null_cnt: 0,
  //           distinctness: 1.0,
  //           unique_cnt: 250,
  //           value_distribution: {
  //             type: "all",
  //             value: [
  //               {
  //                 경기도: 42,
  //               },
  //               {
  //                 서울특별시: 25
  //               },
  //             ],
  //             range: "-"
  //           },
  //         },
  //         string_profile: {
  //           blank_cnt: 0,
  //           min_len: 3,
  //           max_leng: 7,
  //           avg_len: 4.092
  //         }
  //       }
  //     }
  //   ]
  // }

}

async function saveProfilingResult(id, profileResult) {
  const buffer = Buffer.from(JSON.stringify(profileResult));
  const s3Res = await FileManager.uploadJSON(`${id}/${PROFILE_RESULT_S3_KEY}`, buffer);
  console.log("profile result s3 key", s3Res)
}

async function requestProfilingAndSave(dataset, option) {
  try {
    const profileResult = await requestProfileV2(dataset, option);
    if (profileResult) {
      console.log("profile result ==============")
      console.log(profileResult)
      await saveProfilingResult(dataset.id, profileResult);
      
      dataset.hasProfile = true;
      await dataset.save();
    }
  } catch (err) {
    console.error(err);
  }
}

module.exports = { 
  requestProfileV2,
  saveProfilingResult,
  requestProfilingAndSave,
  PROFILE_RESULT_S3_KEY
 };
