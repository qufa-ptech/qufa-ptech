const cron = require("node-cron");

function registerDefaultTasks(taskService) {
  registerSchedule("*/10 * * * * *", function() {
    // Task 처리를 위한 Scheduler 
    taskService.runTask();
    },
    "check task ==================================="
  )
  
  registerSchedule("*/10 * * * * *", function() {
    // Task 완료에 따른 데이터 후처리
    taskService.runPostTask();
    },
    "check post task ==================================="
  )
  
  registerSchedule("*/10 * * * * *", function() {
    // Task 완료에 따른 데이터 후처리
    taskService.runPostProfiling();
    },
    "check profiling task ==================================="
  )
}

function registerSchedule(cronExpression, executeFunction, executeMessage) {
  cron.schedule(cronExpression, async () => {
    try {
      console.log(executeMessage)
      executeFunction();
    } catch (err) {
        console.log(err);
    }
  })
}

module.exports = { 
  registerDefaultTasks,
 };
