const axios = require("axios");
const config = require("../configs/config");
const FileManager = require("../lib/FileManager");
const API_URL = config.improvement.baseUrl;
const TOKEN = config.improvement.token;

async function runDataQualityImprovement(dataset) {

  const key = dataset.remotePath;
  const fileS3DownloadUrl = await FileManager.findS3ObjectUrl(key);

  try {
    // const result = await axios.default.post(
    //   `${API_URL}/api/timebands/run`,
    //   {
    //     id: dataset.id,
    //     fileUrl: fileS3DownloadUrl,
    //   },
    //   {
    //     headers: {
    //       Authorization: `${TOKEN}`,
    //     },
    //   }
    // );

    return result.data;
  } catch (err) {
    console.error(err);
  }

  return null;
}

module.exports = {
  runDataQualityImprovement,
};
