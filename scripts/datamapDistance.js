const { Project, Meta } = require("../models")
const DatamapManager = require("../lib/DatamapManager");
const FileManager = require("../lib/FileManager");
const ProjectService = require("../services/ProjectService")

const projectService = new ProjectService();

run();

async function run() {
  console.log('run datamap distance ==============')
  try {
    const datamapResult = await calcNewDistance();  
    console.log('datamap result ')
    console.log(datamapResult)

    const buffer = Buffer.from(JSON.stringify(datamapResult));
    const s3Res = await FileManager.uploadJSON(`${DatamapManager.DATAMAP_RESULT_S3_KEY}`, buffer);

  } catch (err) {
    console.error(err);
  } finally {
    process.exit(0);
  }
}

async function calcNewDistance() {
  const datamaps = await getDistanceMatrix();
  const datamapResult = {}

  const targetDocIds = Object.keys(datamaps)
  
  for (const targetDocId of targetDocIds) {
    const tempDocResult = {}
    const destDocResult = {}
    
    const destDocs = datamaps[targetDocId]
    const destDocIds = Object.keys(destDocs)
    for (const destDocId of destDocIds) {
      const distance = destDocs[destDocId]
      tempDocResult[`_${destDocId}`] = distance
    }

    
    const sortedEntries = Object.entries(tempDocResult).sort((a, b) => a[1] - b[1]);
    
    let nEntry = 0
    for (const [key, value] of sortedEntries) {
      destDocResult[key] = value

      if (nEntry >= 200) {
        break;
      }

      nEntry++;
    }

    datamapResult[`_${targetDocId}`] = destDocResult
  }

  return datamapResult;
}

async function getDistanceMatrix() {
  const dtf = await getDTF();
  console.log('dtf', dtf);
  const docDist = {}


  const docs = Object.keys(dtf);

  for (const targetDoc of docs) {
    let targetDocDist = {}

    for (const destDoc of docs) {
      if (targetDoc == destDoc) {
        continue;
      }

      const targetTF = dtf[targetDoc]
      const destTF = dtf[destDoc]
      const keywords = Object.keys(targetTF)

      targetDocDist[destDoc] = getEuclidean(targetTF, destTF, keywords)
    }

    docDist[targetDoc] = targetDocDist
  }

  return docDist;
}

async function getDTF() {
  const projects = await projectService.findAllHavingKeywords();

  let keywords = [];
  let dtf = {};
  
  for (const p of projects) {
    if (p.keywords) {
      const list = p.keywords.split(',')
      keywords.push(...list);
    }
  }

  for (const p of projects) {
    let tf = {}

    for(const key of keywords) {
      tf[key] = 0
    }

    const list = p.keywords.split(',');
    for(const key of list) {
      tf[key] = tf[key] + 1
    }

    if (list.length > 0) {
      dtf[p.id] = tf
    }
  }

  return dtf;
}

function getEuclidean(targetDoc, destDoc, keywords) {
  let points = [];
  let numberOfKeywords = 0

  for (const key of keywords) {
    points.push(
      targetDoc[key] - destDoc[key]
    )

    if (targetDoc[key] != 0) {
      numberOfKeywords++;
    }
    if (destDoc[key] != 0) {
      numberOfKeywords++;
    }
  }

  return getDistance(points) / numberOfKeywords;
}

function getDistance(points) {
  let distance = 0.0
  for (const axis of points) {
    distance += Math.pow(axis, 2);
  }
  return Math.sqrt(distance);
}