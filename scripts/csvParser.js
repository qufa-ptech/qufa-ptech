var MetaManager = require("../lib/MetaManager");
var DatasetManager = require("../lib/DatasetManager");
var RawDataManager = require("../lib/RawDataManager");
var ImputationManager = require("../lib/ImputationManager");
var ProfileManager = require("../lib/ProfileManager");

const { Dataset, Meta, MkFeature, Importance } = require("../models");
const config = require("../configs/config");
const MkfeatManager = require("../lib/MkfeatManager");
const TaskService = require("../services/TaskService");

console.log(process.argv);
const args = JSON.parse(process.argv[2]);
console.log(args);

run();

async function run() {
  try {
    const dataset = await Dataset.findOne({
      where: { id: args.datasetId },
      include: [
        {
          model: Meta,
          as: "metas",
        },
      ],
    });
  
    if (!args.parseOption.encoding) {
      args.parseOption.encoding = await MetaManager.detectEncoding(dataset.remotePath);
    }
  
    if (!args.parseOption.delimiter) {
      args.parseOption.delimiter = ",";
    }
  
    await parseCsvAndSave(dataset, args.parseOption);
    await ProfileManager.requestProfilingAndSave(dataset, args.parseOption);
  
  
  } catch (err) {
    console.error(err);
  } finally {
    process.exit(0);
  }
}

async function parseCsvAndSave(dataset, option) {
  const rows = await MetaManager.parseRecord(dataset, option);
  try {
    console.log("Begin Insert Data for " + dataset.originFileName + "========================");
    const startDate = new Date();
    await RawDataManager.insertData(dataset, rows.slice(1));
    const endDate = new Date();
    const seconds = (endDate.getTime() - startDate.getTime()) / 1000;
    const iops = (rows.slice(1).length / seconds).toFixed(2);
    console.log(
      `End Insert Data: ${rows.slice(1).length} rows for ${dataset.originFileName}, 
      ${seconds} seconds,
      ${iops} IOPS
      ========================`
    );
    dataset.status = Dataset.status.done.stat;
    await dataset.save();
  } catch (error) {
    console.log("Exception in insertData =======================");
    console.log(error);
    dataset.status = Dataset.status.error.stat;
    await dataset.save();
  }
}



// 미사용: 확인 후 삭제 예정
async function remainProcess(dataset, option) {
    // v1 code 임시 중지
    // await RawDataManager.enqueueProfile(dataset);

    //Imputation
    // console.log("Call imputation / outlier");
    // await ImputationManager.runImputation(dataset.remotePath);
    // await ImputationManager.runOutlier(dataset.remotePath);

    // mkfeat
    // Feature
    // console.log("MK feat start!");
    // const mkfeatUrl = config.mkfeat.url;
    // const mkfeatManager = new MkfeatManager({ endpoint: mkfeatUrl });
    // const extractData = {
    //   data: {
    //     uri: `s3://qufa-test/${dataset.remotePath}`,
    //     columns: dataset.metas.map((el) => {
    //       return {
    //         name: el.name,
    //         type: el.colType,
    //       };
    //     }),
    //   },
    //   operator: [],
    // };

    // const mkExtractCallBack = async (progress) => {
    //   console.log(`Mkfeat progress: ${progress}`);
    //   dataset.mkfeatProgress = progress;
    //   await dataset.save();
    // };

    // const mkFeatures = await mkfeatManager.batchExtractJob(
    //   extractData,
    //   mkExtractCallBack
    // );

    // const featuresForBulkInsert = mkFeatures.map((el) => {
    //   return {
    //     datasetId: dataset.id,
    //     name: el.name,
    //     colType: el.type,
    //   };
    // });

    // await MkFeature.bulkCreate(featuresForBulkInsert);

    // Importance ------------------------------------------------------------------
    // importance nxn matrix를 생성한다.
    // console.log("Mk importance start!");

    // const dataTypesForImportance = ["number", "boolean"];

    // const targetAvailableMetas = dataset.metas.filter((el) =>
    //   dataTypesForImportance.includes(el.colType)
    // );
    // const targetUnAvailableMetas = dataset.metas.filter(
    //   (el) => !dataTypesForImportance.includes(el.colType)
    // );

    // const inputs = dataset.metas.map((el) => {
    //   const obj = {
    //     id: el.id,
    //     name: el.name,
    //     type: el.colType,
    //   };
    //   return obj;
    // });

    // const rowsForBulkInsert = [];

    // for (let i = 0; i < targetAvailableMetas.length; i++) {
    //   const targetMeta = targetAvailableMetas[i];
    //   for (let j = 0; j < inputs.length; j++) {
    //     if (inputs[j].name == targetMeta.name) {
    //       inputs[j]["label"] = true;
    //     } else {
    //       delete inputs[j]["label"];
    //     }
    //   }

    //   const mkfeatUrl = config.mkfeat.url;
    //   const mkfeatManager = new MkfeatManager({ endpoint: mkfeatUrl });

    //   const uri = `s3://qufa-test/${dataset.remotePath}`;

    //   const payload = {
    //     data: {
    //       uri: uri,
    //       columns: inputs,
    //     },
    //   };

    //   console.log(" Payload ======================");
    //   console.log(inputs);

    //   const results = await mkfeatManager.batchImportanceJob(
    //     payload,
    //     async (progress) => {
    //       const totalProgress =
    //         i * (100 / targetAvailableMetas.length) +
    //         progress / targetAvailableMetas.length;
    //       console.log(`mk importance progress: ${totalProgress}`);
    //       dataset.importanceProgress = totalProgress;
    //       await dataset.save();
    //     }
    //   );

    //   console.log("importance result ========================");
    //   console.log(results);

    //   for (let i = 0; i < inputs.length; i++) {
    //     inputs[i]["importance"] = results[i];
    //   }

    //   //generate result
    //   for (let input of inputs) {
    //     rowsForBulkInsert.push({
    //       targetId: targetMeta.id,
    //       featureId: input.id,
    //       importance: input.importance,
    //     });
    //   }
    // }

    // for (let unableMeta of targetUnAvailableMetas) {
    //   for (let input of inputs) {
    //     rowsForBulkInsert.push({
    //       targetId: unableMeta.id,
    //       featureId: input.id,
    //       importance: 0,
    //     });
    //   }
    // }

    // await Importance.bulkCreate(rowsForBulkInsert);
}
