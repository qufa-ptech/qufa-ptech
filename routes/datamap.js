var express = require("express");
var router = express.Router();
const auth = require("../utils/auth");

const ProjectService = require("../services/ProjectService")
const projectService = new ProjectService();

const DatamapManager = require("../lib/DatamapManager");
const FileManager = require("../lib/FileManager");

router.get(
  "/",
  auth.checkAuth,
  auth.checkRole(1),
  async function (req, res, next) {
    res.render("datamap/index", {
      title: "데이터맵",
      currentMenu: "datamap",
    })
  }
)

router.get(
  "/distances",
  auth.checkAuth,
  auth.checkRole(1),
  async function (req, res, next) {
    const projects = await projectService.findAllHavingKeywords();
    
    try {
      const s3ObjRes = await FileManager.findS3Objct(DatamapManager.DATAMAP_RESULT_S3_KEY);
      const datamapResultJson = JSON.parse(s3ObjRes.Body.toString('utf-8'));
  
      res.json({
        projects: projects,
        datamapResult: datamapResultJson
      })  
    } catch (err) {
      console.error(err);
      res.json(null);
    }
    
  }
)

module.exports = router;